import 'dart:io';

import 'package:logging/logging.dart';

class TcpClient {
  final Logger logger = Logger('TcpClient');

  final TcpClientListener listener;
  final String DELIMITER = '\r\n';

  Socket _socket;
  String _message = "";

  TcpClient(this.listener);

  void start(String ip, int port) async {
    if (_socket != null) {
      _socket.destroy();
    }
    logger.info("Open socket $ip:$port");
    _socket = await Socket.connect(ip, port, timeout: Duration(seconds: 10));
    _socket.listen(_handleMessage);
    logger.info("Socket opened");
  }

  void _handleMessage(List<int> data) async {
    String parsedData = String.fromCharCodes(data);
    _message += parsedData;
    if (_message.endsWith(DELIMITER)) {
      _message.split(DELIMITER).forEach((m) {
        if (m.isNotEmpty) {
          logger.info('Receive : $m');
          listener.onMessageReceived(m);
        }
      });
      _message = '';
    }
  }

  void stop() {
    _socket.destroy();
    _socket = null;
  }

  bool isConnected() {
    if (_socket == null) return false;
    //FIXME
    return true; //_socket.isConnected();
  }

  void sendMessage(final String message) {
    logger.info('Send : $message');
    _socket.add(message.codeUnits);
  }
}

abstract class TcpClientListener {
  void onMessageReceived(String message);

  void onConnecting();

  void onConnected();

  void onDisconnected(Exception e);
}
