import 'package:snapdart/models/group.dart';
import 'package:snapdart/models/server.dart';
import 'package:snapdart/models/snap_stream.dart';

class Status {
  List<SnapStream> streams;
  List<Group> groups;
  Server server;

  Status() {
    streams = List();
    groups = List();
  }

  Status.fromJson(Map<String, dynamic> json)
      : server = Server.fromJson(json['server']),
        groups =
            (json['groups'] as List).map((g) => Group.fromJson(g)).toList(),
        streams = (json['streams'] as List)
            .map((g) => SnapStream.fromJson(g))
            .toList();
}
