import 'package:snapdart/models/stream_uri.dart';

class SnapStream {
  StreamUri uri;
  String id;
  String status;

  SnapStream.fromJson(Map<String, dynamic> json)
      : uri = StreamUri.fromJson(json['uri']),
        id = json['id'],
        status = json['status'];
}
