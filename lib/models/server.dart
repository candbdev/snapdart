import 'package:snapdart/models/host.dart';
import 'package:snapdart/models/snap_server.dart';

class Server {
  Host host;
  SnapServer snapserver;

  Server.fromJson(Map<String, dynamic> json)
      : snapserver = SnapServer.fromJson(json['snapserver']),
        host = Host.fromJson(json['host']);
}
