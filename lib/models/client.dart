import 'package:snapdart/models/client_config.dart';
import 'package:snapdart/models/host.dart';
import 'package:snapdart/models/snap_time.dart';
import 'package:snapdart/models/snapcast.dart';

class Client {
  Host host;
  Snapcast snapclient;
  ClientConfig config;

  SnapTime lastSeen;
  bool connected;
  String id;
  bool deleted = false;

  Client.fromJson(Map<String, dynamic> json)
      : config = ClientConfig.fromJson(json['config']),
        snapclient = Snapcast.fromJson(json['snapclient']),
        connected = json['connected'],
        id = json['id'],
        deleted = json['deleted'],
        lastSeen = SnapTime.fromJson(json['lastSeen']),
        host = Host.fromJson(json['host']);
}
