class SnapTime {
  int sec = 0;
  int usec = 0;

  SnapTime.fromJson(Map<String, dynamic> json)
      : sec = json['sec'],
        usec = json['usec'];
}
