class Volume {
  bool muted;
  int percent;

  Volume.fromJson(Map<String, dynamic> json)
      : muted = json['muted'],
        percent = json['percent'];
}
