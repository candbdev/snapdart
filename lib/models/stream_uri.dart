class StreamUri {
  String raw;
  String scheme;
  String host;
  String path;
  String fragment;
  Map<String, String> query;

  StreamUri.fromJson(Map<String, dynamic> json)
      : raw = json['raw'],
        scheme = json['scheme'],
        host = json['host'],
        path = json['path'],
        fragment = json['fragment'],
        query =  Map.from(json['query']);
}
