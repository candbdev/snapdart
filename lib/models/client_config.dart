import 'package:snapdart/models/volume.dart';

class ClientConfig {
  String name;
  Volume volume;
  int latency;
  int instance;

  ClientConfig.fromJson(Map<String, dynamic> json)
      : name = json['name'],
        volume = Volume.fromJson(json['volume']),
        latency = json['latency'],
        instance = json['instance'];
}
