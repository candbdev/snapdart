class Host {
  String name;
  String mac;
  String os;
  String arch;
  String ip;

  Host.fromJson(Map<String, dynamic> json)
      : name = json['name'],
        mac = json['mac'],
        os = json['os'],
        arch = json['arch'],
        ip = json['ip'];
}
