class SnapServer {
  String name;
  String version;
  int controlProtocolVersion;
  int protocolVersion;

  SnapServer.fromJson(Map<String, dynamic> json)
      : name = json['name'],
        version = json['version'],
        controlProtocolVersion = json['controlProtocolVersion'],
        protocolVersion = json['protocolVersion'];
}
