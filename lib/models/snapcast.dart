class Snapcast {
  String name = "";
  String version = "";
  int protocolVersion = 1;

  Snapcast.fromJson(Map<String, dynamic> json)
      : name = json['name'],
        version = json['version'],
        protocolVersion = json['protocolVersion'];
}
