import 'package:snapdart/models/client.dart';

class Group {
  String name;
  String id;
  bool muted;
  String streamId;
  List<Client> clients;

  Group.fromJson(Map<String, dynamic> json)
      : name = json['name'],
        id = json['id'],
        muted = json['muted'],
        streamId = json['stream_id'],
        clients =
            (json['clients'] as List).map((c) => Client.fromJson(c)).toList();
}
