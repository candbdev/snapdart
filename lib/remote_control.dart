import 'dart:collection';
import 'dart:convert';

import 'package:logging/logging.dart';
import 'package:snapdart/models/client.dart';
import 'package:snapdart/models/group.dart';
import 'package:snapdart/models/status.dart';
import 'package:snapdart/models/volume.dart';
import 'package:snapdart/snapdart.dart';
import 'package:snapdart/tcp_client.dart';

const int SNAPCAST_CONTROL_PORT = 1705;
const String DELIMITER = '\r\n';
const String SERVER_GETSTATUS = 'Server.GetStatus';
const String SERVER_GETRPCVERSION = 'Server.GetRPCVersion';
const String SERVER_DELETECLIENT = 'Server.DeleteClient';
const String SERVER_ONUPDATE = 'Server.OnUpdate';
const String CLIENT_GETSTATUS = 'Client.GetStatus';
const String CLIENT_SETNAME = 'Client.SetName';
const String CLIENT_SETLATENCY = 'Client.SetLatency';
const String CLIENT_SETSTREAM = 'Client.SetStream';
const String CLIENT_SETVOLUME = 'Client.SetVolume';
const String CLIENT_ONCONNECT = 'Client.OnConnect';
const String CLIENT_ONDISCONNECT = 'Client.OnDisconnect';
const String CLIENT_ONVOLUMECHANGED = 'Client.OnVolumeChanged';
const String CLIENT_ONLATENCYCHANGED = 'Client.OnLatencyChanged';
const String CLIENT_ONNAMECHANGED = 'Client.OnNameChanged';
const String GROUP_GETSTATUS = 'Group.GetStatus';
const String GROUP_SETMUTE = 'Group.SetMute';
const String GROUP_SETSTREAM = 'Group.SetStream';
const String GROUP_SETCLIENTS = 'Group.SetClients';
const String GROUP_SETNAME = 'Group.SetName';
const String GROUP_ONMUTE = 'Group.OnMute';
const String GROUP_ONSTREAMCHANGED = 'Group.OnStreamChanged';
const String GROUP_ONNAMECHANGED = 'Group.OnNameChanged';
const String STREAM_ONUPDATE = 'Stream.OnUpdate';
const String STREAM_ADDSTREAM = 'Stream.AddStream';
const String STREAM_REMOVESTREAM = 'Stream.RemoveStream';

class RemoteControl implements TcpClientListener {
  final Logger logger = Logger('RemoteControl');

  final Map<int, RPCRequest> pendingRequests = HashMap();
  final List<RemoteControlListener> _remoteControlListener = List();

  TcpClient _tcpClient;
  int msgId = 1;

  RemoteControl() {
    _tcpClient = TcpClient(this);
  }

  addListener(RemoteControlListener remoteControlListener) {
    this._remoteControlListener.add(remoteControlListener);
  }

  removeListener(RemoteControlListener remoteControlListener) {
    this._remoteControlListener.remove(remoteControlListener);
  }

  start(String ip) async {
    await _tcpClient.start(ip, 1705);
  }

  @override
  void onMessageReceived(String message) {
    try {
      Map<String, dynamic> json = jsonDecode(message);

      if (json.containsKey('error')) {
        return;
      }

      //answer to a request
      if (json.containsKey('id')) {
        var id = json['id'];
        String method;
        if (pendingRequests.containsKey(id)) {
          method = pendingRequests[id].method;
        } else {
          return;
        }

        switch (method) {
          case SERVER_DELETECLIENT:
          case GROUP_SETCLIENTS:
          case SERVER_GETSTATUS:
            _remoteControlListener.forEach((r) => r.onUpdateServerStatus(
                Status.fromJson(json['result']['server'])));
            break;
          case CLIENT_SETLATENCY:
            _remoteControlListener.forEach((r) => r.onLatencyChanged(
                pendingRequests[id].params['id'], json['result']['latency']));
            break;
          case CLIENT_SETVOLUME:
            _remoteControlListener.forEach((r) => r.onVolumeChanged(
                pendingRequests[id].params['id'],
                Volume.fromJson(json['result']['volume'])));
            break;
          case CLIENT_SETNAME:
            _remoteControlListener.forEach((r) => r.onNameChanged(
                pendingRequests[id].params['id'], json['result']['name']));
            break;
          case GROUP_SETNAME:
            _remoteControlListener.forEach((r) => r.onGroupNameChanged(
                pendingRequests[id].params['id'], json['result']['name']));
            break;
          case GROUP_SETMUTE:
            _remoteControlListener.forEach((r) => r.onMute(
                pendingRequests[id].params['id'], json['result']['mute']));
            break;
          case GROUP_SETSTREAM:
            _remoteControlListener.forEach((r) => r.onStreamChanged(
                pendingRequests[id].params['id'], json['result']['stream_id']));
            break;
          case STREAM_ADDSTREAM:
            _remoteControlListener
                .forEach((r) => r.onStreamAdded(json['result']['stream_id']));
            break;
          case STREAM_REMOVESTREAM:
            _remoteControlListener
                .forEach((r) => r.onStreamRemoved(json['result']['stream_id']));
            break;
        }
      } else {
        switch (json['method']) {
          case CLIENT_ONVOLUMECHANGED:
            _remoteControlListener.forEach((r) => r.onVolumeChanged(
                json['params']['id'],
                Volume.fromJson(json['params']['volume'])));
            break;
          case CLIENT_ONNAMECHANGED:
            _remoteControlListener.forEach((r) =>
                r.onNameChanged(json['params']['id'], json['params']['name']));
            break;
          case GROUP_ONSTREAMCHANGED:
            _remoteControlListener.forEach((r) => r.onStreamChanged(
                json['params']['id'], json['params']['stream_id']));
            break;
          case GROUP_ONMUTE:
            _remoteControlListener.forEach(
                (r) => r.onMute(json['params']['id'], json['params']['mute']));
            break;
          case SERVER_ONUPDATE:
            _remoteControlListener.forEach((r) => r.onUpdateServerStatus(
                Status.fromJson(json['params']['server'])));
            break;
          case STREAM_ONUPDATE:
            _remoteControlListener.forEach((r) => r.onUpdateStream(
                json['params']['id'],
                SnapStream.fromJson(json['params']['stream'])));
            break;
          case CLIENT_ONCONNECT:
            _remoteControlListener.forEach(
                (r) => r.onConnect(Client.fromJson(json['params']['client'])));
            break;
          case CLIENT_ONDISCONNECT:
            _remoteControlListener
                .forEach((r) => r.onConnect(json['params']['id']));
            break;
          case CLIENT_ONLATENCYCHANGED:
            _remoteControlListener.forEach((r) => r.onLatencyChanged(
                json['params']['id'], json['params']['latency']));
            break;
          case GROUP_ONNAMECHANGED:
            _remoteControlListener.forEach((r) => r.onGroupNameChanged(
                json['params']['id'], json['params']['name']));
            break;
        }
      }
    } catch (e) {
      logger.severe(e.toString());
      _remoteControlListener.forEach((r) => r.onSnapcastError());
    }
  }

  RPCRequest _jsonRequest(String method, Map params) {
    var rpcRequest = RPCRequest(method, params, msgId);
    pendingRequests[msgId] = rpcRequest;
    msgId++;
    return rpcRequest;
  }

  void deleteClient(String clientId) {
    Map params = HashMap();
    params['id'] = clientId;

    RPCRequest request = _jsonRequest(SERVER_DELETECLIENT, params);
    _tcpClient.sendMessage(jsonEncode(request) + DELIMITER);
  }

  void setClients(String groupId, List<String> clientIds) {
    Map params = HashMap();
    params['id'] = groupId;
    params['clients'] = clientIds;

    RPCRequest request = _jsonRequest(GROUP_SETCLIENTS, params);
    _tcpClient.sendMessage(jsonEncode(request) + DELIMITER);
  }

  void setMute(String groupId, bool muted) {
    Map params = HashMap();
    params['id'] = groupId;
    params['mute'] = muted;

    RPCRequest request = _jsonRequest(GROUP_SETMUTE, params);
    _tcpClient.sendMessage(jsonEncode(request) + DELIMITER);
  }

  void setLatency(String clientId, int latency) {
    Map params = HashMap();
    params['id'] = clientId;
    params['latency'] = latency;

    RPCRequest request = _jsonRequest(CLIENT_SETLATENCY, params);
    _tcpClient.sendMessage(jsonEncode(request) + DELIMITER);
  }

  void setName(String clientId, String name) {
    Map params = HashMap();
    params['id'] = clientId;
    params['name'] = name;

    RPCRequest request = _jsonRequest(CLIENT_SETNAME, params);
    _tcpClient.sendMessage(jsonEncode(request) + DELIMITER);
  }

  void setGroupName(String groupId, String name) {
    Map params = HashMap();
    params['id'] = groupId;
    params['name'] = name;

    RPCRequest request = _jsonRequest(GROUP_SETNAME, params);
    _tcpClient.sendMessage(jsonEncode(request) + DELIMITER);
  }

  void getServerStatus() {
    RPCRequest request = _jsonRequest(SERVER_GETSTATUS, null);
    _tcpClient.sendMessage(jsonEncode(request) + DELIMITER);
  }

  void setVolume(String clientId, int volume, bool muted) {
    Map params = HashMap();
    params['id'] = clientId;
    params['volume'] = {
      'muted': muted,
      'percent': volume,
    };

    RPCRequest request = _jsonRequest(CLIENT_SETVOLUME, params);
    _tcpClient.sendMessage(jsonEncode(request) + DELIMITER);
  }

  void setStreamGroup(String streamId, String groupId) {
    Map params = HashMap();
    params['id'] = groupId;
    params['stream_id'] = streamId;

    RPCRequest request = _jsonRequest(GROUP_SETSTREAM, params);
    _tcpClient.sendMessage(jsonEncode(request) + DELIMITER);
  }

  void addStream(String streamUri) {
    Map params = HashMap();
    params['streamUri'] = streamUri;

    RPCRequest request = _jsonRequest(STREAM_ADDSTREAM, params);
    _tcpClient.sendMessage(jsonEncode(request) + DELIMITER);
  }

  void removeStream(String streamId) {
    Map params = HashMap();
    params['id'] = streamId;

    RPCRequest request = _jsonRequest(STREAM_REMOVESTREAM, params);
    _tcpClient.sendMessage(jsonEncode(request) + DELIMITER);
  }

  @override
  void onDisconnected(Exception e) {}

  @override
  void onConnected() {}

  @override
  void onConnecting() {}
}

class RPCRequest {
  String method;
  Map params;
  int id;

  RPCRequest(this.method, this.params, this.id);

  Map<String, dynamic> toJson() => params == null
      ? {'id': id, 'jsonrpc': '2.0', 'method': method}
      : {'id': id, 'jsonrpc': '2.0', 'method': method, 'params': params};
}

abstract class RemoteControlListener {
  void onSnapcastError();

  void onConnect(Client client);

  void onDisconnect(String clientId);

  void onVolumeChanged(String clientId, Volume volume);

  void onLatencyChanged(String clientId, double latency);

  void onNameChanged(String clientId, String name);

  void onGroupNameChanged(String groupId, String name);

  void onMute(String groupId, bool mute);

  void onStreamChanged(String groupId, String streamId);

  void onUpdate(Client client);

  void onUpdateStream(String streamId, SnapStream stream);

  void onUpdateServerStatus(Status server);

  void onUpdateGroup(Group group);

  void onConnected(RemoteControl remoteControl);

  void onConnecting(RemoteControl remoteControl);

  void onDisconnected(RemoteControl remoteControl, Exception e);

  void onBatchStart();

  void onBatchEnd();

  void onStreamAdded(String streamId);

  void onStreamRemoved(String streamId);
}
