# 0.1.4

fix: improve handling when receiving multiple message

# 0.1.3

feat: handle all requests and notification from snapcast protocol
feat: add exemple

# 0.1.2

Fix bug on deserilization

# 0.1.1

Code improvement

# 0.1.0

Initial Version of the library.

- Include the possibility to connect to a Snapcast Server and make some basic request