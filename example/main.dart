import 'package:snapdart/snapdart.dart';

import 'SnapcastClient.dart';

void main() {
  demo();
}

void demo() async{
    RemoteControl remoteControl = RemoteControl();
  SnapcastClient snapcastClient = SnapcastClient();
  await remoteControl.start('192.168.1.116');
  remoteControl.addListener(snapcastClient);
  remoteControl.getServerStatus();

}
