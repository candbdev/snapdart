import 'package:snapdart/snapdart.dart';

class SnapcastClient implements RemoteControlListener {
  void onSnapcastError() {}

  void onConnect(Client client) {}

  void onDisconnect(String clientId) {}

  void onVolumeChanged(String clientId, Volume volume) {
    print(clientId + " " + volume.percent.toString());
  }

  void onLatencyChanged(String clientId, double latency) {}

  void onNameChanged(String clientId, String name) {}

  void onGroupNameChanged(String groupId, String name) {}

  void onMute(String groupId, bool mute) {}

  void onStreamChanged(String groupId, String streamId) {}

  void onUpdate(Client client) {}

  void onUpdateStream(String streamId, SnapStream stream) {}

  void onUpdateServerStatus(Status server) {
    print(server.server.host.name);
    print(server.server.hashCode);
  }

  void onUpdateGroup(Group group) {}

  void onConnected(RemoteControl remoteControl) {}

  void onConnecting(RemoteControl remoteControl) {}

  void onDisconnected(RemoteControl remoteControl, Exception e) {}

  void onBatchStart() {}

  void onBatchEnd() {}

  void onStreamAdded(String streamId) {}

  void onStreamRemoved(String streamId) {}
}
